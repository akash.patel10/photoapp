from distutils.command.upload import upload
from sys import flags
from unicodedata import name
from django.db import models
from datetime import datetime
# Create your models here.

class Tag(models.Model):
    name = models.CharField(max_length=100, null = False, blank = False)

    def __str__(self):
        return self.name

class Photo(models.Model):
    image = models.ImageField(upload_to = 'images')

    date = models.DateField(default=datetime.now, blank=True)

    title = models.CharField(max_length=100,null = True)

    def __str__(self):
        return self.description