# from http.client import HTTPResponse
# from http.client import HTTPResponse

from tkinter import Image
from django.shortcuts import render,redirect,HttpResponse

from .models import Photo
from .forms import ImageForm

def gallery(request):
    # logger.info('here goes your message')
    if(request.method == 'GET'): 
        images = Photo.objects.all().order_by('date')
    return render(request,'photos/gallery.html',{'images' : images})

def addPhoto(request):
    form = ImageForm()
    if request.method == 'POST':
       form  = ImageForm(request.POST,request.FILES)
       if form.is_valid():
        form.save()
       return redirect('/')
    return render(request,'photos/add.html',{'form' : form})

def search(request):
    query = request.GET['query']
    images = Photo.objects.filter(title__icontains = query)

    params = {'images' : images} 

    return render(request,'photos/search.html',params)