from operator import imod
from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers 


# from photoApp.photos.views import gallery
from . import views

urlpatterns = [
    path('', views.gallery,name='gallery'),
    path('upload',views.addPhoto,name='add'),
    path('search',views.search,name='search')

]