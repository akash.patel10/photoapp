from turtle import width
from django import forms
from .models import Photo

class ImageForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = '__all__'
        widgets = {
           'title' : forms.TextInput(attrs={'class' : 'form-control'}), 
           'image' : forms.FileInput(attrs={'class' : 'form-control'}) ,
           'date' : forms.DateInput(attrs={'class' : 'form-control'})             
        }